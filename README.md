Startheque
==========

Le site de la \*thèque

Démarrage rapide
----------------

Pour déployer le site :
```
make deploy
```

Pour générer le site statique :
```
make site
```

Dépendances
-----------

Les dépendances nécessaires à la génération du site sont listées dans
[requirements-dev.txt](requirements-dev.txt) :
- [yamlpp](https://www.lri.fr/~filliatr/yamlpp.fr.html) : pour gérer les
  différentes langues.
