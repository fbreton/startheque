inv=open("inventaire_bd.txt",encoding="ISO-8859-1")
out=open("out.txt","w")
livres=inv.readlines()
liste=[]
for livre in livres:
    livre=livre[:-1].split(";")
    presente=True
    if " j" in livre:#On exclut les doublons (qui sont normalement quelque part à Jourdan)
        presente=False
    for e in livre:
        if e.find("m=20")!=-1:#On exclut les livres manquants
            presente=False
    if presente:
        liste.append(livre)
for livre in sorted(liste):
    out.write('<li><span class="bookauthor">'+livre[0]+'</span>, <span class="booktitle">'+livre[1]+'</span></li>\n')
out.close()