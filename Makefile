HERE=$(shell pwd)
SITE_DIR=$(HERE)/public
SRC_DIR=$(HERE)/src
DEPLOY_DIR=$(HOME)/www

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'


.PHONY: all
all: site deploy ## Generate the site and deploy it


.PHONY: deploy
deploy: ## Deploy the site
	rm -rf $(DEPLOY_DIR)
	ln --symbolic --force $(SITE_DIR) $(DEPLOY_DIR)


.PHONY: site
site: $(SITE_DIR)/index.html $(SITE_DIR)/index.fr.html $(SITE_DIR)/index.en.html ## Generate the site

$(SITE_DIR)/%.html: $(SITE_DIR)/%.fr.html
	cp $< $@

$(SITE_DIR)/%.fr.html: $(SRC_DIR)/%.prehtml
	yamlpp -l fr -o $@ $<

$(SITE_DIR)/%.en.html: $(SRC_DIR)/%.prehtml
	yamlpp -l en -o $@ $<

