function search(){
    var debut=Date.now();
    var reqs=document.getElementById("search").value.toUpperCase().split(" ").filter(x=>x.length!=0);
    var liste=document.getElementById("liste");
    var li=liste.getElementsByTagName("li");
    var auths=[].slice.call(document.getElementsByClassName("bookauthor")).map(x=>x.innerText);
    var titles=[].slice.call(document.getElementsByClassName("booktitle")).map(x=>x.innerText);
    for(var i=0;i<li.length;i++){
        var bookauthor=auths[i];
        var booktitle=titles[i];
        var found=true;
        for(var keyword of reqs){
            keyword=" "+keyword
            var place=" "+bookauthor.toUpperCase()+" "+booktitle.toUpperCase();
            keyword=keyword.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            place=place.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            if(place.indexOf(keyword)==-1){
                found=false;
            }
        }
        if(found){
            li[i].style.display="";
        }else{
            li[i].style.display="none";
        }
    }
    console.log(Date.now()-debut);
}

function debounce(callback, delay){
    var timer;
    return function(){
        var args = arguments;
        var context = this;
        clearTimeout(timer);
        timer = setTimeout(function(){
            callback.apply(context, args);
        }, delay)
    }
}

var debouncedSearch=debounce(search,200);
